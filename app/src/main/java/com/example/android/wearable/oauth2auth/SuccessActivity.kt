package com.example.android.wearable.oauth2auth

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class SuccessActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success)

        val successAuth = findViewById<TextView>(R.id.text_access)
        successAuth.text = intent.getStringExtra("token")
    }
}