package com.example.android.wearable.oauth2auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import com.example.android.wearable.oauth2auth.databinding.ActivityMainBinding
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val client_id = "f8d5920e3485ca04adee"
    private val secret_id = "8560d3187b3040fefaf98ac48a2e7ae411d48b01"
    private val baseUrl = "https://github.com/login/oauth/authorize"
    private val redirect_url = "https://auth.ru/"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.buttonLogin.setOnClickListener {
            authSend()
        }
    }

    private fun authSend(){
        binding.webView.visibility = View.VISIBLE
        binding.webView.settings.javaScriptEnabled = true
        val load_url = "$baseUrl?client_id=$client_id&redirect_uri=$redirect_url&scope=repo"
        binding.webView.loadUrl(load_url)
        binding.webView.settings.allowFileAccess = true
        binding.webView.settings.allowContentAccess = true
        binding.buttonLogin.visibility = View.GONE
        binding.webView.webViewClient = object : WebViewClient(){
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                if (request?.url?.host.toString() == "auth.ru") {
                    if ("code" in request?.url.toString()) {
                        val requestCode = request?.url?.getQueryParameter("code")
                        authRequest(requestCode.toString())
                        return true
                    } else {
                        binding.buttonLogin.visibility = View.VISIBLE
                        binding.webView.visibility = View.GONE
                        notAuth()
                        return true
                    }
                } else {
                    return false
                }
            }
        }
    }

    private fun notAuth() {
        Toast.makeText(this, "Вы отклонили авторизацию", Toast.LENGTH_SHORT).show()
    }

    private fun retrofitBuilder(): MyApiInterface {
        val logi = HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY)
            .setLevel(HttpLoggingInterceptor.Level.BASIC)
            .setLevel(HttpLoggingInterceptor.Level.HEADERS)
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(logi)
            .build()

        return Retrofit.Builder()
            .baseUrl("https://github.com/")
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(MyApiInterface::class.java)
    }

    private fun authRequest(code: String) {
        val result = retrofitBuilder().authorizeRequest(client_id, secret_id, code)
        result.enqueue(object : retrofit2.Callback<ResponseAccess>{
            override fun onResponse(
                call: Call<ResponseAccess>,
                response: Response<ResponseAccess>
            ) {
                Log.d("ResponseAuth", response.body().toString())
                val intent = Intent(this@MainActivity, SuccessActivity::class.java)
                intent.putExtra("token", response.body()!!.access_token)
                startActivity(intent)
            }

            override fun onFailure(call: Call<ResponseAccess>, t: Throwable) {
                Log.d("fail", t.localizedMessage.toString())
            }
        })
        Log.d("AuthRequest", code)
    }
}